from django import forms
from django.contrib.auth import get_user_model
from kommons.utils.http import get_request_site

from kauth.models import Account, VerificationLog
from kauth.validators import is_valid_email, is_valid_phone
from kauth.constants import KauthK


class VerificationSendForm(forms.Form):
    address = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))

    def __init__(self, **kwargs):
        self.request = kwargs.pop('request', None)
        self.verification_medium = kwargs.pop('verification_medium', {})
        super().__init__( **kwargs)

        self.fields['address'].label = self.verification_medium['label']
        help_text = self.verification_medium.get('help_text', None)
        if help_text:
            self.fields['address'].help_text = help_text

    def clean_address(self):
        val = self.cleaned_data['address']
        is_valid = False

        key = self.verification_medium.get('key', None)
        if key == KauthK.VERIFICATION_METHOD_EMAIL.value:
            is_valid = is_valid_email(val)
        elif key == KauthK.VERIFICATION_METHOD_SMS.value:
            is_valid = is_valid_phone(val)
            val = (val[1:] if val[0] == '+' else val)  # Remove + from number

        if not is_valid:
            raise forms.ValidationError('Please enter a valid value')

        return val

    def clean(self):
        cd = self.cleaned_data
        address = cd.get('address', None)
        if address:
            filters = {}

            key = self.verification_medium.get('key', None)
            if key == KauthK.VERIFICATION_METHOD_EMAIL.value:
                filters = {'email': address}
            elif key == KauthK.VERIFICATION_METHOD_SMS.value:
                filters = {'phone': address}

            site = get_request_site(self.request)
            qs = Account.objects.filter(site=site, **filters)
            if qs.exists():
                cd['account'] = qs.first()
            else:
                raise forms.ValidationError('No existing record found with the value provided.')

        return cd


class VerificationConfirmForm(forms.Form):
    verification_code = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))

    def __init__(self, **kwargs):
        self.request = kwargs.pop('request', None)
        self.verification_log = kwargs.pop('verification_log', None)
        super().__init__( **kwargs)

        if self.verification_log.verification.type == KauthK.VERIFICATION_TYPE_PASSWORD_RESET.value:
            self.fields['new_password'] = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                                                          min_length=4, help_text='Minimum length: 4')

    def clean_verification_code(self):
        val = self.cleaned_data['verification_code']

        if not self.verification_log:
            raise forms.ValidationError('Verification process not available')
        else:
            if self.verification_log.key != val:
                raise forms.ValidationError('Verification code not found.')

        return val


class UserManageForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class ': 'form-control'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class ': 'form-control'}), min_length=4,
                               help_text='Minimum length: 4')
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={'class ': 'form-control'}), min_length=4,
                                       help_text='Minimum length: 4')

    def clean_username(self):
        val = self.cleaned_data['username']

        qs = get_user_model().objects.filter(username=val)
        if qs.exists():
            raise forms.ValidationError('Username already exists')

        return val

    def clean_confirm_password(self):
        val = self.cleaned_data['confirm_password']
        password = self.cleaned_data['password']

        if password != val:
            raise forms.ValidationError('Passwords need to be the same')

        return val
