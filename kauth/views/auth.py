from django.views.generic.edit import FormView
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from django.contrib.auth import get_user_model
from django.contrib import messages

from kommons.views import ReferrerRedirectView
from kommons.utils.generic import boolify
from kommons.utils.model import get_object_from_pk, get_model_from_str
from kauth.forms import UserManageForm
from kauth.utils import GenerateAccountUtils


class UserManageCardView(ReferrerRedirectView, FormView):
    template_name = 'kauth/auth/user_manage.html'
    form_class = UserManageForm
    related_object = None
    referrer_prefix = 'usermanagecardview'

    def dispatch(self, request, *args, **kwargs):
        self.related_object = self.get_related_object()
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        return {
            'username': self.request.GET.get('username') or ''
        }

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']

        user = get_user_model().objects.create_user(username=username, password=password, is_active=True)

        user_field = self.kwargs['user_field']
        if self.related_object:
            setattr(self.related_object, user_field, user)
            self.related_object.save(update_fields=[user_field])

        messages.success(self.request, 'User created successfully')
        self.success_url = self.get_referrer() or '/'

        return super().form_valid(form)

    def get_referrer_key(self):
        if self.related_object:
            return f'{self.referrer_prefix}-{self.related_object.pk}'
        return self.referrer_prefix

    def get_related_object(self):
        app_model = self.kwargs['app_model']
        pk = self.kwargs['pk']

        return get_object_from_pk(app_model, pk)


class UserGenerateAccountsView(ReferrerRedirectView, TemplateView):
    template_name = 'kauth/auth/generate_accounts.html'
    referrer_prefix = 'usergenerateaccount'
    objects = []

    def dispatch(self, request, *args, **kwargs):
        self.objects = self.get_objects()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        gen_account_utils = GenerateAccountUtils(request)
        password_option = request.POST.get('password_option')
        force_reset = request.POST.get('force_reset')
        force_reset = boolify(force_reset)

        for obj in self.get_queryset():
            username = request.POST.get(f'{obj.pk}__username')
            has_account = request.POST.get(f'{obj.pk}__has_account')
            has_account = boolify(has_account)

            if not has_account or force_reset:
                account = gen_account_utils.generate_user_account(obj, username, self.kwargs['user_field'],
                                                                  password_option=password_option)

        messages.success(self.request, 'Generated accounts successfully')
        url = self.get_referrer() or '/'
        return HttpResponseRedirect(url)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)

        ctx.update({
            'objects': self.objects
        })
        return ctx

    def get_queryset(self):
        model_class = get_model_from_str(self.kwargs['app_model'])
        if model_class:
            pk_str = self.request.GET.get('pks', '')
            pks = pk_str.split(',')
            return model_class.objects.filter(pk__in=pks)

    def get_objects(self):
        qs = self.get_queryset()
        xs = []
        for obj in qs:
            user = getattr(obj, self.kwargs['user_field'], None)
            if user:
                has_account = True
                username = user.username
            else:
                has_account = False
                username = obj.generate_username()

            xs.append({
                'pk': obj.pk,
                'name': str(obj),
                'username': username,
                'has_account': has_account
            })
        return xs



