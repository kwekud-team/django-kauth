from django.urls import reverse
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.shortcuts import get_object_or_404
from django.contrib import messages

from kauth.forms import VerificationSendForm, VerificationConfirmForm
from kauth.utils import get_verification_mediums, get_verification_medium, VerificationUtils
from kauth.models import VerificationLog
from kauth.constants import KauthK


class VerificationView(TemplateView):
    template_name = 'kauth/verification/options.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update({
            'title': 'Password Reset',
            'verification_mediums': get_verification_mediums(),
            'verification_type': self.kwargs['type']
        })
        return context


class VerificationSendView(FormView):
    template_name = 'kauth/verification/send.html'
    form_class = VerificationSendForm
    verification_medium = None

    def dispatch(self, request, *args, **kwargs):
        self.verification_medium = self.get_verification_medium()
        return super(VerificationSendView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update({
            'title': 'Verification',
            'verification_medium': self.verification_medium,
            'verification_type': self.kwargs['type']
        })
        return context

    def get_form_kwargs(self):
        dt = super().get_form_kwargs()
        dt['request'] = self.request
        dt['verification_medium'] = self.verification_medium
        return dt

    def form_valid(self, form):
        address = form.cleaned_data['address']
        account = form.cleaned_data['account']

        verify_utils = VerificationUtils(self.request, account)
        vlog = verify_utils.create_verification(self.kwargs['type'], self.kwargs['medium'], address)

        self.success_url = reverse('kauth:verification_confirm', args=(vlog.pk,))
        return super().form_valid(form)

    def get_verification_medium(self):
        return get_verification_medium(self.kwargs['medium'])


class VerificationConfirmView(FormView):
    template_name = 'kauth/verification/confirm.html'
    form_class = VerificationConfirmForm
    verification_log = None
    verify_utils = None

    def dispatch(self, request, *args, **kwargs):
        self.verification_log = self.get_verification_log()
        self.verify_utils = VerificationUtils(self.request, self.verification_log.verification.account)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update({
            'title': 'Confirm',
            'verification_medium': get_verification_medium(self.verification_log.verification.medium),
            'verification_type': self.verification_log.verification.type,
            'verification_log': self.verification_log
        })
        return context

    def get_form_kwargs(self):
        dt = super().get_form_kwargs()
        dt['request'] = self.request
        dt['verification_log'] = self.verification_log
        return dt

    def get_verification_log(self):
        return get_object_or_404(VerificationLog, pk=self.kwargs['vlog_pk'])

    def get(self, request, *args, **kwargs):
        action = request.GET.get('action', None)
        if action == 'resend':
            self.verify_utils.send_verification(self.verification_log.verification)
            messages.success(self.request, "Message resent successfully.")

        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        verification_code = form.cleaned_data['verification_code']

        if self.verification_log.verification.type == KauthK.VERIFICATION_TYPE_PASSWORD_RESET.value:
            new_password = form.cleaned_data['new_password']
            self.verify_utils.set_password(self.verification_log, new_password)
            messages.success(self.request, 'Password changed successfully. Please login with your new password')
            self.success_url = '/'

        return super().form_valid(form)
