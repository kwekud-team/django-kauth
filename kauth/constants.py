from enum import Enum


class KauthK(Enum):
    VERIFICATION_TYPE_NEW_USER = 'new-user'
    VERIFICATION_TYPE_PASSWORD_RESET = 'password-reset'

    VERIFICATION_METHOD_EMAIL = 'email'
    VERIFICATION_METHOD_SMS = 'sms'
