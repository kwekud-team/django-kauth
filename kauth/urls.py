from django.urls import path

from kauth.views import auth, verification


app_name = 'kauth'

urlpatterns = [
    path('verification/all/<str:type>/', verification.VerificationView.as_view(), name='verification'),
    path('verification/send/<str:type>/<str:medium>/', verification.VerificationSendView.as_view(),
         name='verification_send'),
    path('verification/confirm/<str:vlog_pk>/', verification.VerificationConfirmView.as_view(),
         name='verification_confirm'),

    path('auth/user-manage/<str:app_model>/<int:pk>/<str:user_field>/', auth.UserManageCardView.as_view(),
         name='user_manage_add'),

    path('auth/genereate-accounts/<str:app_model>/<str:user_field>/', auth.UserGenerateAccountsView.as_view(),
         name='user_generate_accounts'),
]
