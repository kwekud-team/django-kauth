from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import reverse

from kommons.utils.model import get_str_from_model
from kauth.models import Account, Verification, VerificationLog


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ('site', 'user', 'email', 'phone', 'last_login', 'last_logout', 'login_role', 'password_option',
                    'date_verified')
    search_fields = ['user__username',]


@admin.register(Verification)
class VerificationAdmin(admin.ModelAdmin):
    list_display = ("account", "type", "medium", "address", 'key', 'date_sent')


@admin.register(VerificationLog)
class VerificationLogAdmin(admin.ModelAdmin):
    list_display = ("verification", "date_sent", "key", "address", 'expiry_date', 'date_expired', 'date_verified')


class KauthAdmin(admin.ModelAdmin):
    actions = ['generate_account']
    kauth_user_field = 'user'

    def generate_account(self, request, queryset):
        pks = ','.join([str(x) for x in queryset.values_list('pk', flat=True)])
        app_model = get_str_from_model(self.model)
        base_url = reverse('kauth:user_generate_accounts', args=(app_model, self.kauth_user_field,))
        return HttpResponseRedirect(f'{base_url}?pks={pks}')
    generate_account.short_description = 'Generate User Account'
