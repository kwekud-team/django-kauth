from random import randint
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth import get_user_model

from kommons.utils.http import get_request_site
from kmsg.dclasses import TemplateData
from kmsg.utils import send_out_mail, send_out_sms
from kauth.models import Account
from kauth.constants import KauthK
from kauth.models import Verification, VerificationLog
from kauth.signals import kauth_account_generated


def get_verification_mediums():
    return {
        KauthK.VERIFICATION_METHOD_EMAIL.value: {
            'label': 'Email', 'icon_class': 'fa fa-envelope',
            'description': 'Use this option to get instructions on how to reset your password in your email'
        },
        KauthK.VERIFICATION_METHOD_SMS.value: {
            'label': 'Phone', 'icon_class': 'fa fa-phone',
            'description': 'Use this option to get password reset code as an SMS.',
            'help_text': 'Eg: 233201234567'
        },
    }


def get_verification_medium(key):
    medium = get_verification_mediums().get(key, {})
    if medium:
        medium['key'] = key
    return medium


class VerificationUtils:

    def __init__(self, request, account):
        self.request = request
        self.account = account

    def create_verification(self, verification_type, verification_medium, address):

        verification = Verification.objects.update_or_create(
            account=self.account,
            type=verification_type,
            medium=verification_medium,
            defaults={
                'address': address,
                'key': '---'
            })[0]

        return self.send_verification(verification)

    def create_verification_log(self, verification):
        verification_log = VerificationLog.objects.filter(
            verification=verification,
            address=verification.address,
            date_verified__isnull=True
        ).first()

        if not verification_log:
            verification_log = VerificationLog.objects.create(
                verification=verification,
                address=verification.address,
                key=self.generate_verify_code()
            )

        return verification_log

    def send_verification(self, verification):
        verification_log = self.create_verification_log(verification)

        response = self._send(verification_log)
        now = (timezone.now() if response.is_valid else None)

        Verification.objects.filter(pk=verification_log.verification.pk).update(key=verification_log.key)
        VerificationLog.objects.filter(pk=verification_log.pk).update(date_sent=now, response=response.to_dict())

        return verification_log

    @staticmethod
    def generate_verify_code():
        return randint(100000, 999999)

    def _send(self, verification_log):
        recipients = [verification_log.verification.address]
        url = reverse('kauth:verification_confirm', args=(verification_log.pk,))
        url = self.request.build_absolute_uri(url)

        state = verification_log.verification.type.replace('-', '_')
        template_data = TemplateData(code='kauth_verification', state=state, extra_context={'url': url})
        user = verification_log.verification.account.user

        if verification_log.verification.medium == KauthK.VERIFICATION_METHOD_SMS.value:
            response = send_out_sms(self.request, recipients, template_data, content_object=verification_log, user=user)
        else:
            response = send_out_mail(self.request, recipients, template_data, content_object=verification_log, user=user)

        return response

    def set_password(self, verification_log, password):
        if verification_log.verification.type == KauthK.VERIFICATION_TYPE_PASSWORD_RESET.value:
            user = verification_log.verification.account.user
            user.set_password(password)
            user.save()

            verification_log.date_verified = timezone.now()
            verification_log.save(update_fields=['date_verified'])

            return True


class GenerateAccountUtils:

    def __init__(self, request):
        self.request = request

    def generate_user_account(self, instance, username, user_field, password_option='random'):
        user = getattr(instance, user_field, None)

        if not user:
            account = None
            instance_model = instance._meta.model
            user_model = get_user_model()

            user = user_model.objects.filter(username=username).first()
            if user:
                # Check if user belongs to a different instance
                if instance_model.objects.filter(user=user).exists():
                    # do nothing for now. we can't really auto change user account to someone else
                    pass
                else:
                    setattr(instance, user_field, user)
                    instance.save()
                    account = self.create_kauth_account(instance, user, '', password_option)
            else:
                password = self.get_password(instance, password_option)
                user = user_model.objects.create_user(username, password=password, is_staff=True, is_active=True)
                setattr(instance, user_field, user)
                instance.save()

                account = self.create_kauth_account(instance, user, password, password_option)

            kauth_account_generated.send(sender=Account, request=self.request, related_instance=instance,
                                         account=account)

        return account

    def get_password(self, instance, option):
        password = ''

        # bf = generate password by field on instance
        if option.startswith('bf__'):
            field_name = option.split('__')[-1]
            password = getattr(instance, field_name, None)
            if password:
                password = password.lower()

        if not password:
            password = get_user_model().objects.make_random_password(length=5)

        return password

    def create_kauth_account(self, instance, user, password, password_option):
        site = get_request_site(self.request)

        defaults = {
            'login_role': instance._meta.model_name.lower(),
            'password_option': password_option
        }
        if password:
            defaults['temp_pass'] = password

        return Account.objects.update_or_create(
            site=site,
            user=user,
            defaults=defaults
        )[0]
