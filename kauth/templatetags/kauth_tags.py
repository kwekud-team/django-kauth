from django import template
from kdash.utils import DashUtils

register = template.Library()


@register.inclusion_tag('kauth/auth/includes/user_card.html', takes_context=True)
def show_user_card(context, related_object, user_field='user', proposed_username=''):
    request = context['request']

    ctx = {
        'related_object': related_object,
    }

    if request.user.has_perm('auth.change_user'):
        try:
            user_record = getattr(related_object, user_field)
            is_valid_related = True
        except AttributeError:
            user_record = None
            is_valid_related = False

        ctx.update({
            'proposed_username': proposed_username,
            'has_perm': True,
            'related_object': related_object,
            'user_field': user_field,
            'user_record': user_record,
            'is_valid_related': is_valid_related
        })

    return ctx
