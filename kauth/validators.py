import re
import phonenumbers


def is_valid_email(value):
    regex = r'^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
    return True if re.search(regex, value) else False


def is_valid_phone(value):
    value = ('+' + value if value[:0] != '+' else value)

    try:
        phone_obj = phonenumbers.parse(value)
        phonenumbers.format_number(phone_obj, phonenumbers.PhoneNumberFormat.E164)
        is_valid = phonenumbers.is_valid_number(phone_obj)
    except phonenumbers.NumberParseException:
        is_valid = False

    return is_valid


# def format_phone(phone_number):
#     val = str(phone_number).lstrip('0')
#     val = ('+' + val if val[:0] != '+' else val)
#
#     try:
#         x = phonenumbers.parse(val, None)
#     except phonenumbers.NumberParseException:
#         x = None
#
#     if x:
#         val = phonenumbers.format_number(x, phonenumbers.PhoneNumberFormat.INTERNATIONAL)
#
#     return val
