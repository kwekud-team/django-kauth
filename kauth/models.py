from django.db import models
from django.conf import settings
from django.contrib.auth.models import User


class Account(models.Model):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    last_login = models.DateTimeField(blank=True, null=True)
    last_logout = models.DateTimeField(blank=True, null=True)
    date_verified = models.DateTimeField(null=True, blank=True)
    phone = models.CharField(max_length=50, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    login_role = models.CharField(max_length=50, null=True, blank=True)
    password_option = models.CharField(max_length=50, null=True, blank=True)
    temp_pass = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return f'{self.site} - {self.user}'

    class Meta:
        unique_together = ['site', 'user']


def get_kauth_account(user, site):
    if site:
        return Account.objects.get_or_create(user=user, site=site)[0]


User.kauth_account = get_kauth_account


class Verification(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    type = models.CharField(max_length=50)
    medium = models.CharField(max_length=20)
    address = models.CharField(max_length=100)
    key = models.CharField(max_length=50, blank=True)  # For reference. key in VerificationLog table is used in funcs
    date_sent = models.DateTimeField(null=True, blank=True)
    # date_verified = models.DateTimeField(null=True, blank=True)
    # date_created = models.DateTimeField(auto_now_add=True)
    # date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.account} - {self.type}'

    class Meta:
        unique_together = ['account', 'type', 'medium']


class VerificationLog(models.Model):
    verification = models.ForeignKey(Verification, on_delete=models.CASCADE)
    date_sent = models.DateTimeField(null=True, blank=True)
    key = models.CharField(max_length=50, blank=True)
    address = models.CharField(max_length=100)
    expiry_date = models.DateTimeField(null=True, blank=True)
    date_expired = models.DateTimeField(null=True, blank=True)
    date_verified = models.DateTimeField(null=True, blank=True)
    response = models.JSONField(default=dict)

    def __str__(self):
        return f'{self.date_sent}'
