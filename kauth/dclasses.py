from dataclasses import dataclass


@dataclass
class Response:
    is_valid: bool
    resp_code: str
    resp_msg: str

    def to_dict(self):
        return {
            'is_valid': self.is_valid,
            'resp_code': self.resp_code,
            'resp_msg': self.resp_msg
        }
